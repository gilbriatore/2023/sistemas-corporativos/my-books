# My Books

Exemplos de uma livaria construída com Spring Boot e Thymeleaf.

## Baixar o projeto...

Fazer o download do zip ou clonar com:

```
git clone https://gitlab.com/gilbriatore/2023/sistemas-corporativos/my-books.git
```

## Baixar o IntelliJ IDEA Community Edition...


- [ ] [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download)


Instalar a IDE, abrir os projetos e executar.