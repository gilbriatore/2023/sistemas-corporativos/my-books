package briatore.livraria.util;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import briatore.livraria.modelo.Livro;

@Component
public class PopulaBanco {
	
	@PersistenceContext
	EntityManager manager;
	
	@Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;
	
	
	/*
	 * Método é executado automaticamente quando o Spring sobe o contexto da aplicação
	 */
    @PostConstruct
    private void init(){
    	//métodos @PostConstruct não são transacional, por isso gerenciamos a TX manualmente
        TransactionTemplate tmpl = new TransactionTemplate(txManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
            	PopulaBanco.this.roda();
            }
        });
   }

	public void roda() { 
		Livro livro1 = geraLivro(
				"ARQUITETURA",
				"Agile Software Development, Principles, Patterns, and Practices",
				"Agile Software Development",
				"Robert C. Martin",
				"Written by a software developer for software developers, this book is a unique collection of the latest "
				+ "software development methods. The author includes OOD, UML, Design Patterns, Agile and XP methods with "
				+ "a detailed description of a complete software design for reusable programs in C++ and Java. Using a practical, "
				+ "problem-solving approach, it shows how to develop an object-oriented application—from the early stages "
				+ "of analysis, through the low-level design and into the implementation.",
				"agile_software_development.jpg", "19.30", "38.90");
		
		Livro livro2 = geraLivro(
				"ARQUITETURA",
				"Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions",
				"Enterprise Integration Patterns",
				"Gregor Hohpe",
				"Enterprise Integration Patterns provides an invaluable catalog of sixty-five patterns, with "
				+ "real-world solutions that demonstrate the formidable of messaging and help you to design "
				+ "effective messaging solutions for your enterprise.",
				"enterprise_integration.jpg", "29.90", "69.90");

		manager.persist(livro1);
		manager.persist(livro2);
		
	}

	private static Livro geraLivro(String codigo, String titulo, String tituloCurto,
			String nomeAutor, String descricao, String imagem,
			String valorEbook, String valorImpresso) {

		return new Livro(codigo, titulo, tituloCurto, descricao, nomeAutor, imagem,
				new BigDecimal(valorEbook), new BigDecimal(valorImpresso));
	}
}
